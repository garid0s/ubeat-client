define(function () {
    "use strict";

    var Config = {
        url: {
            apiUrl: "https://ubeat.herokuapp.com/unsecure",
            securedApiUrl: "https://ubeat.herokuapp.com"
//          securedApiUrl: "http://localhost:3000",
//          apiUrl: "http://localhost:3000/unsecure"
        },

        rootFolder: "/projet-web/"
    };

    return Config;
});
