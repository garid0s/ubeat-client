requirejs.config({
    paths: {
        jquery: "http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min",
        "jquery.cookie": "http://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie",
        underscore: "http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min",
        backbone: "http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min",
        text: "http://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text",
        css: "js/libs/css.min",
        cryptoJS: "http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5"
    },
    map: {
        "css": "js/libs/css.min"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "jquery.cookie": {
            deps: ["jquery"]
        },
        "cryptoJS": {
            exports: "CryptoJS"
        }
    }
});

require([
    "jquery",
    "backbone",
    "config",
    "js/views/footerView",
    "js/views/header/headerView",
    "js/router/appRouter",
    "js/audio/AudioPlayer",
    "jquery.cookie"
], function ($, Backbone, Config, FooterView, HeaderView, AppRouter, AudioPlayer) {
    "use strict";

    var initiateRouter = function () {
        new AppRouter();

        Backbone.history.start({
            pushState: false,
            root: Config.rootFolder
        });
    };

    var header = new HeaderView(initiateRouter);

    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (jqxhr.status == 401) {
            window.location.href = "#homeLogin";

            var signUpFormOn = $("#signUpFormContainer").css("max-height") == "500px";
            header.user.clear();

            if (signUpFormOn) {
                header.showSignUpForm()
            }
            else {
                header.showLoginForm();
            }
        }
    });

    new FooterView();

    AudioPlayer.setControls($("#currentSongPlayer")[0]);
});

