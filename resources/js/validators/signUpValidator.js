define(function () {
    "use strict";

    var signUpValidator = function (attributes) {
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var errors = {
            "email": "",
            "name": "",
            "password": ""
        };
        if (attributes.email === "") {
            errors.email = "Email cannot be empty.";
        }

        else if (attributes.email && !emailPattern.test(attributes.email)) {
            errors.email = "Invalid email format.";
        }
        if (attributes.name === "") {
            errors.name = "Name cannot be empty.";
        }

        if (attributes.password === "") {
            errors.password = "Password cannot be empty.";
        }

        else if (attributes.password !== attributes.confirmPassword) {
            errors.password = "Password confirmation failed.";
        }
        if (errors.email || errors.name || errors.password) {
            return errors;
        }
    };

    return signUpValidator;
});
