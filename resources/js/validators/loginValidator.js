define(function () {
    "use strict";

    var loginValidator = function (attributes) {
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var errors = {
            "email": "",
            "password": ""
        };
        if (attributes.email === "") {
            errors.email = "Email cannot be empty.";
        }

        else if (attributes.email && !emailPattern.test(attributes.email)) {
            errors.email = "Invalid email format.";
        }

        if (attributes.password === "") {
            errors.password = "Password cannot be empty.";
        }

        if (errors.email || errors.password) {
            return errors;
        }
    };

    return loginValidator;
});
