define([
        "cryptoJS"
    ],
    function (md5) {
        "use strict";

        var Gravatar = {
            getGravatarImageURL: function (email, size) {
                var hashedEmail = this.hashEmail(email);
                return "http://www.gravatar.com/avatar/" + hashedEmail + "?s=" + size + "&r=pg";
            },

            hashEmail: function (email) {
                var hashedEmail = email.trim();
                hashedEmail = hashedEmail.toLowerCase();
                hashedEmail = CryptoJS.MD5(hashedEmail);
                return hashedEmail.toString();
            }
        };

        return Gravatar;
    });