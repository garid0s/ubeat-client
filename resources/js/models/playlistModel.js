define([
    "backbone",
    "config",
], function (Backbone, Config) {
    "use strict";

    var PlaylistModel = Backbone.Model.extend({
        urlRoot: Config.url.securedApiUrl + "/playlists/",

        defaults: {
            "name": "",
            "numberOfSongs": "0",
            "tracks": []
        },

        initialize: function () {
            this.url = this.urlRoot + this.id;
        },

        parse: function (response) {
            var newNumberOfSongs = this.getNewNumberOfSongs(response);
            this.url = this.urlRoot + response.id;

            if (typeof response.name === "undefined" || response.name === "") {
                response.name = "-";
            }

            var hash = {
                "name": response.name,
                "id": response.id,
                "numberOfSongs": newNumberOfSongs,
                "tracks": response.tracks
            };

            return hash;
        },

        validate: function (attributes) {
            if (attributes.name === "") {
                return "The name cannot be empty!"
            }
        },

        getNewNumberOfSongs: function (response) {
            var newNumberOfSongs;

            if (response.tracks) {
                newNumberOfSongs = response.tracks.length;
            }
            else {
                newNumberOfSongs = this.get("numberOfSongs");
            }
            return newNumberOfSongs;
        }
    });

    return PlaylistModel;

});

