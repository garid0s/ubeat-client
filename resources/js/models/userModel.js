define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/gravatar"
], function ($, _, Backbone, Config, Utilities, Gravatar) {
    "use strict";

    var UserModel = Backbone.Model.extend({
        default: {
            "email": "",
            "name": "",
            "password": "",
            "userId": "",
            "token": "",
            "gravatarImage": ""
        },

        initialize: function (specificPath) {
            this.url = Config.url.securedApiUrl + specificPath;
        },

        parse: function (response) {
            this.addFollowingUserImages(response.following);
            var hash = {
                email: response.email,
                name: response.name,
                userId: response.id,
                token: response.token,
                following: response.following,
                smallGravatarImage: Gravatar.getGravatarImageURL(response.email, 50),
                gravatarImage: Gravatar.getGravatarImageURL(response.email, 60),
                bigGravatarImage: Gravatar.getGravatarImageURL(response.email, 100),
                veryBigGravatarImage: Gravatar.getGravatarImageURL(response.email, 200)
            };
            return hash;
        },

        addFollowingUserImages: function (following) {
            _.each(following, function (user) {
                user["gravatarImage"] = Gravatar.getGravatarImageURL(user["email"], 60);
                user["bigGravatarImage"] = Gravatar.getGravatarImageURL(user["email"], 100);
            });
        },

        isFollowing: function (otherUserId) {
            for (var i = 0; i < this.attributes.following.length; i++) {
                if (this.attributes.following[i].id === otherUserId) {
                    return true;
                }
            }

            return false;
        },

        saveInCookies: function () {
            $.cookie(Utilities.TOKEN_COOKIE, this.attributes.token);
            $.cookie(Utilities.USER_EMAIL, this.attributes.email);
            $.cookie(Utilities.USER_ID, this.attributes.userId);

            $.ajaxSetup({
                headers: {"Authorization": this.attributes.token}
            });
        },

        deleteUserCookies: function () {
            $.removeCookie(Utilities.TOKEN_COOKIE);
            $.removeCookie(Utilities.USER_EMAIL);
            $.removeCookie(Utilities.USER_ID);

            $.ajaxSetup({
                headers: {"Authorization": ""}
            });
        },

        followUser: function (callback) {
            var self = this;
            $.ajax(Config.url.securedApiUrl + "/follow", {
                type: "POST",
                data: {
                    id: self.attributes.userId
                }
            }).done(callback);

        },

        unfollowUser: function (callback) {
            var self = this;
            $.ajax(Config.url.securedApiUrl + "/follow/" + self.attributes.userId, {
                type: "DELETE",
                data: {
                    id: self.attributes.userId
                }
            }).done(callback);
        }
    });

    return UserModel;
});