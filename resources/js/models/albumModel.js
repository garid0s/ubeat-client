define([
    "backbone",
    "config",
    "js/utils"
], function (Backbone, Config, Utilities) {
    "use strict";

    var AlbumModel = Backbone.Model.extend({

        urlRoot: Config.url.apiUrl + "/albums/",

        defaults: {
            "albumId": "",
            "albumArtist": "",
            "albumGenre": "",
            "albumName": "",
            "albumReleaseDate": "",
            "albumCoverImageMediumResUrl": "",
            "albumCoverImageHighResUrl": "",
            "albumLinkUrl": ""
        },

        initialize: function () {
            this.url = this.urlRoot + this.id;
        },

        parse: function (response) {
            var result = response;

            if (typeof response.results !== "undefined" && response.results instanceof Array) {
                result = response.results[0];
            }

            var hash = {
                "albumId": result.collectionId,
                "albumName": result.collectionName,
                "albumArtist": result.artistName,
                "albumArtistId": result.artistId,
                "albumGenre": result.primaryGenreName,
                "albumReleaseDate": new Date(result.releaseDate).getFullYear(),
                "albumCoverImageLowResUrl": result.artworkUrl100,
                "albumCoverImageMediumResUrl": Utilities.imgUrlTo400x400(result.artworkUrl100),
                "albumCoverImageHighResUrl": Utilities.imgUrlTo600x600(result.artworkUrl100),
                "albumCoverImageVeryHighResUrl": Utilities.imgUrlTo1200x1200(result.artworkUrl100),
                "albumLinkUrl": result.collectionViewUrl
            };

            return hash;
        }
    });

    return AlbumModel;
});

