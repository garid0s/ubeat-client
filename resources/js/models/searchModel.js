define([
    "backbone",
    "config"
], function (Backbone, Config) {
    "use strict";

    var SearchModel = Backbone.Model.extend({

        default: {
            "name": "",
            "subName": "",
            "link": "",
            "coverImage": "",
            "id": "",
            "type": ""
        },

        initialize: function (option) {
            this.url = Config.url.apiUrl + "?q=" + option;
        },

        parse: function (response) {
            var hash = {
                coverImage: response.artworkUrl60
            };
            this.setSearchDescription(hash, response);

            return hash;
        },

        setSearchDescription: function (hash, response) {
            if (response.wrapperType == "collection") {
                hash.name = response.collectionName;
                hash.subName = response.artistName;
                hash.link = "#/albums/" + response.collectionId;
                hash.type = response.wrapperType;
                hash.itemId = response.collectionId;
            }
            else if (response.wrapperType == "track") {
                hash.name = response.trackName;
                hash.subName = response.collectionName + ", " + response.artistName;
                hash.link = "#/albums/" + response.collectionId;
                hash.type = response.wrapperType;
                hash.itemId = response.trackId;
                hash.trackData = response;
            }
            else if (response.wrapperType == "artist") {
                hash.name = response.artistName;
                hash.link = "#/artists/" + response.artistId;
                hash.type = response.wrapperType;
                hash.itemId = response.artistId;
            }
            else {
                hash.name = "unknown";
            }
        }

    });

    return SearchModel;
});