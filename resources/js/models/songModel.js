define([
    "backbone",
    "js/utils"
], function (Backbone, Utilities) {
    "use strict";

    var SongModel = Backbone.Model.extend({
        default: {
            "songName": "",
            "songNumber": "",
            "songLength": "",
            "songPreviewUrl": "",
            "songCollectionName": "",
            "songArtwork": "",
            "songArtist": "",
            "id": ""
        },

        parse: function (response) {
            var hash = {
                "result": response,
                "songName": response.trackName,
                "songNumber": response.trackNumber,
                "songLength": Utilities.transformMillisToMinutes(response.trackTimeMillis),
                "songPreviewUrl": response.previewUrl,
                "songCollectionName": response.collectionName,
                "songArtwork": response.artworkUrl100,
                "songArtist": response.artistName,
                "songId": response.trackId,
                "id": response._id
            };

            return hash;
        }
    });

    return SongModel;
});
