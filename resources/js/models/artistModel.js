define([
    "backbone",
    "config"
], function (Backbone, Config) {
    "use strict";

    var ArtistModel = Backbone.Model.extend({

        urlRoot: Config.url.apiUrl + "/artists/",

        defaults: {
            "artistName": "",
            "primaryGenreName": "",
            "artistLinkUrl": ""
        },

        initialize: function () {
            this.url = this.urlRoot + this.id;
        },

        parse: function (response) {
            var hash = {
                "artistName": response.results[0].artistName,
                "primaryGenreName": response.results[0].primaryGenreName,
                "artistLinkUrl": response.results[0].artistLinkUrl
            };
            return hash;
        }
    });

    return ArtistModel;
});
