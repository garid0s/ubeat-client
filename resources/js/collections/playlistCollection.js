define([
    "backbone",
    "config",
    "js/utils",
    "js/models/playlistModel"
], function (Backbone, Config, Utilities, playlistModel) {
    "use strict";

    var PlaylistCollection = Backbone.Collection.extend({
        model: playlistModel,
        url: Config.url.securedApiUrl + "/playlists",
        userEmail: "",

        parse: function (response) {
            var hash = [];
            var userEmail = "";
            if (this.userEmail === "") {
                userEmail = $.cookie(Utilities.USER_EMAIL);
            }
            else {
                userEmail = this.userEmail;
            }
            $.each(response, function (i, playlist) {
                if (playlist.owner && playlist.owner.email === userEmail) {
                    hash.push(playlist)
                }
            });
            return hash;
        },

        setUserEmail: function (email) {
            this.userEmail = email;
        }
    });

    return PlaylistCollection;
});
