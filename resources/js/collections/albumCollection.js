define([
    "backbone",
    "js/models/albumModel"
], function (Backbone, AlbumModel) {
    "use strict";

    var AlbumCollection = Backbone.Collection.extend({
        model: AlbumModel,

        parse: function (response) {
            return response.results;
        }
    });

    return AlbumCollection;
});
