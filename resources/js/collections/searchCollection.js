define([
    "backbone",
    "js/models/searchModel",
    "config"
], function (Backbone, SearchModel, Config) {
    "use strict";

    var SearchCollection = Backbone.Collection.extend({
        model: SearchModel,
        url: Config.url.apiUrl + "/search",
        searchItem: "",
        searchType: "",

        initialize: function (options) {
            if (options && options.item) {
                this.searchItem = options.item;

                if (options.type) {
                    this.searchType = options.type;

                    if (options.type === "albums" || options.type === "tracks" || options.type === "artists") {

                        this.url += "/" + options.type;
                    }
                }

                this.url += "?q=" + this.searchItem;

                if (options.limit) {
                    this.url += "&limit=" + options.limit;
                }
            }
        },

        parse: function (response) {
            return response.results;
        },

        findByAttribute: function (attribute, value) {
            var foundModel = "undefined";

            this.each(function (model) {
                if (model.get(attribute) === value) {
                    foundModel = model;
                }
            });

            return foundModel;
        }
    });

    return SearchCollection;
});