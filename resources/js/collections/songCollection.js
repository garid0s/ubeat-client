define([
    "backbone",
    "js/models/songModel",
    "js/audio/AudioTrack"
], function (Backbone, SongModel, AudioTrack) {
    "use strict";

    var SongCollection = Backbone.Collection.extend({
        model: SongModel,

        parse: function (response) {

            if (response.tracks) {
                return response.tracks;
            }

            return response.results;
        },

        getAllTracks: function () {
            var trackArray = [];

            this.forEach(function (song) {
                trackArray.push(song.get("result"));
            }, this);
            return trackArray;
        }
    });

    return SongCollection;
});
