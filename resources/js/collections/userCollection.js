define([
    "backbone",
    "js/models/userModel",
    "config",
    "js/utils"
], function (Backbone, UserModel, Config, Utilities) {
    "use strict";

    var UserCollection = Backbone.Collection.extend({
        model: UserModel,
        url: Config.url.securedApiUrl + "/search/users",
        searchItem: "",
        limit: 15,

        initialize: function (options) {
            if (options && options.item) {
                this.url += "?q=" + options.item;
                this.searchItem = options.item;
            }

            if (options && options.limit) {
                this.limit = options.limit;
            }
        },
        parse: function (response) {
            return response.slice(0, this.limit);
        },

        sync: function (method, model, options) {
            //refact http://stackoverflow.com/questions/16472593/override-backbone-sync-at-model-level-to-send-extra-params
            options.headers = {
            "Authorization": $.cookie(Utilities.TOKEN_COOKIE)
            };
            return Backbone.sync.apply(this, arguments);
        }
    });

    return UserCollection;
});