define([
    "jquery",
    "underscore",
    "backbone",
    "js/collections/playlistCollection",
    "text!templates/menu/playlistMenuTemplate.html",
    "css!style/playlistContextMenu.css"
], function ($, _, Backbone, PlaylistCollection, PlaylistMenuTemplate) {
    var PlaylistMenuView = Backbone.View.extend({
        template: _.template(PlaylistMenuTemplate),
        el: "#playlistMenuContainer",

        events: {
            "click li": "playlistMenuItem"
        },

        selectedPlaylistId: "",
        createNewPlaylistSelection: false,

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            this.collection = new PlaylistCollection();

            this.collection.bind("sync", self.render);
        },

        render: function () {
            this.$el.html(this.template({
                playlists: this.collection.toJSON()
            }));

            this.delegateEvents();
        },

        playlistMenuItem: function (event) {
            var playListIndex = $(event.target).index();

            if (playListIndex < this.collection.length) {
                this.selectedPlaylistId = this.collection.at(playListIndex).id;
            }
            else {
                this.createNewPlaylistSelection = true;
            }
        },

        resetMenu: function () {
            this.selectedPlaylistId = "";
            this.createNewPlaylistSelection = false;
        }
    });

    return PlaylistMenuView;
});