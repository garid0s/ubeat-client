define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/views/menu/playlistMenuView",
    "js/collections/songCollection",
    "js/collections/userCollection",
    "js/models/userModel",
    "text!templates/search/searchPageTemplate.html",
    "css!style/searchPage.css",
    "css!style/userFriends.css"
], function ($, _, Backbone, Config, Utilities, PlaylistMenuView, SongCollection, UserCollection,
             UserModel, SearchPageTemplate) {
    var ResearchView = Backbone.View.extend({
        template: _.template(SearchPageTemplate),
        events: {
            "click .result": "redirectToSpecificResultPage",
            "click .searchAddPlaylist": "addToPlaylist",
            "click .followUserButton": "modifyFriends"
        },

        el: "#tabContainer",
        playlistMenuView: "",
        userCollection: "",
        currentUserModel: "",

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            this.userCollection = new UserCollection();
            this.playlistMenuView = new PlaylistMenuView();

            this.collection.bind("sync", function () {
                if (self.collection.searchType === "users") {
                    self.collection.reset();
                }

                if ($.cookie(Utilities.TOKEN_COOKIE) && self.isAUserSearch()) {
                    self.userCollection.url += "?q=" + encodeURIComponent(self.collection.searchItem);
                    self.userCollection.fetch().done(function () {
                        var userModel = new UserModel("/users/" + $.cookie(Utilities.USER_ID));
                        userModel.fetch().done(function () {
                            self.currentUserModel = userModel;
                            self.render();
                        });
                    });
                }
                else {
                    self.userCollection.reset();
                    self.render();
                }
            });
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        render: function () {
            this.$el.html(this.template({
                researchModel: this.collection.toJSON(),
                researchItem: this.collection.searchItem,
                researchType: this.collection.searchType,
                userLoggedIn: $.cookie(Utilities.TOKEN_COOKIE),
                currentUserModel: this.currentUserModel,
                users: this.userCollection.toJSON()
            }));

            this.setFilterTypeFromSearchType();

            if ($.cookie(Utilities.TOKEN_COOKIE)) {
                this.playlistMenuView.$el = this.$("#playlistMenuContainer");
                this.playlistMenuView.collection.fetch();
            }

            this.delegateEvents();
        },

        redirectToSpecificResultPage: function (event) {
            var url = $(event.target).data("url");

            if (typeof url === "undefined") {
                url = $(event.target).parents(".result").data("url");
            }

            window.location.href = url;
        },

        modifyFriends: function (event) {
            event.stopPropagation();

            var userId = $(event.target).parents(".result").data("id");
            var userModel = new UserModel("/users/" + userId);

            userModel.fetch().done(function () {
                var currentUser = new UserModel("/users/" + $.cookie(Utilities.USER_ID));
                currentUser.fetch().done(function () {
                    if (currentUser.isFollowing(userId)) {
                        userModel.unfollowUser(function () {
                            $(event.target).text("+Follow");
                        });

                    }
                    else {
                        userModel.followUser(function () {
                            $(event.target).text("Unfollow");
                        });
                    }
                });
            });
        },

        addToPlaylist: function (event) {
            event.stopPropagation();

            var type = $(event.target).parents(".result").data("type");
            var id = $(event.target).parents(".result").data("id");

            if (type === "track") {
                this.addSongToPlaylist(id);
            }
            else {
                this.addAlbumToPlaylist(id);
            }
        },

        addSongToPlaylist: function (trackId) {
            var songData = this.collection.findByAttribute("itemId", trackId).attributes.trackData;

            var that = this;
            var addSongToPlayListCallbackAfterClick = function () {
                if (that.playlistMenuView.selectedPlaylistId) {
                    that.addSelectedSongToSpecificPlaylist(songData);
                }
                else if (that.playlistMenuView.createNewPlaylistSelection) {
                    that.createNoNamePlaylist("song", songData);
                }
            };

            this.displayPlaylistMenu(addSongToPlayListCallbackAfterClick);
        },

        addAlbumToPlaylist: function (albumId) {
            var that = this;

            var addAlbumToPlaylistCallbackAfterClick = function () {
                var songCollection = new SongCollection();
                songCollection.url = Config.url.apiUrl + "/albums/" + albumId + "/tracks";

                songCollection.fetch().done(function () {
                    if (that.playlistMenuView.selectedPlaylistId) {
                        that.addAlbumToSpecificPlaylist(songCollection);
                    }
                    else if (that.playlistMenuView.createNewPlaylistSelection) {
                        that.createNoNamePlaylist("album", songCollection);
                    }

                    that.playlistMenuView.resetMenu();
                });
            };

            this.displayPlaylistMenu(addAlbumToPlaylistCallbackAfterClick);
        },

        addSelectedSongToSpecificPlaylist: function (songData) {
            $.ajax({
                type: "POST",
                url: this.playlistMenuView.collection.url + "/" +
                this.playlistMenuView.selectedPlaylistId + "/tracks",
                data: songData,
                dataType: "json",

                success: function (data) {
                },

                error: function (data) {
                }
            });
        },

        addAlbumToSpecificPlaylist: function (songCollection) {
            songCollection.models.forEach(function (song) {
                this.addSelectedSongToSpecificPlaylist(song.attributes.result);
            }, this);
        },

        createNoNamePlaylist: function (itemToAdd, data) {
            var that = this;

            $.ajax(Config.url.securedApiUrl + "/tokenInfo", {
                type: "GET"
            }).done(function (owner) {
                that.playlistMenuView.collection.create({
                        "name": "No Name",
                        "owner": owner
                    },
                    {
                        type: "POST",
                        url: that.playlistMenuView.collection.url,

                        success: function (playlist) {
                            that.playlistMenuView.selectedPlaylistId = playlist.id;
                            that.playlistMenuView.render();
                            if (itemToAdd === "album") {
                                that.addAlbumToSpecificPlaylist(data);
                            }
                            else {
                                that.addSelectedSongToSpecificPlaylist(data)
                            }
                        }
                    });
            });
        },

        displayPlaylistMenu: function (callback) {
            var menu = $("#playlistMenuContainer");
            menu.removeClass("hidden");
            menu.css("left", event.pageX);
            menu.css("top", event.pageY - 80);

            var that = this;
            var callbackOnMenuExit = function () {
                callback();
                $("#playlistMenuContainer").addClass("hidden");
                $("html").unbind("click", callbackOnMenuExit);
            };

            $("html").on("click", callbackOnMenuExit);
        },

        setFilterTypeFromSearchType: function () {
            if (this.collection.searchType === "albums") {
                $("#filterAll").removeClass("selectedFilter");
                $("#filterAlbum").addClass("selectedFilter");
            }
            else if (this.collection.searchType === "tracks") {
                $("#filterAll").removeClass("selectedFilter");
                $("#filterTrack").addClass("selectedFilter");
            }
            else if (this.collection.searchType === "artists") {
                $("#filterAll").removeClass("selectedFilter");
                $("#filterArtist").addClass("selectedFilter");
            }
            else if (this.collection.searchType === "users") {
                $("#filterAll").removeClass("selectedFilter");
                $("#filterUser").addClass("selectedFilter");
            }
        },

        isAUserSearch: function () {
            return this.collection.searchType === "" || this.collection.searchType === "users";
        }
    });

    return ResearchView;
});