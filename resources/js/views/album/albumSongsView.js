define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils.js",
    "js/audio/AudioTrack",
    "js/audio/AudioPlayer",
    "js/views/menu/playlistMenuView",
    "text!templates/album/albumSongsTemplate.html"
], function ($, _, Backbone, Config, Utilities, AudioTrack, AudioPlayer, PlaylistMenuView, AlbumSongsTemplate) {
    var AlbumSongsView = Backbone.View.extend({
        template: _.template(AlbumSongsTemplate),
        events: {
            "click .albumSongNumber": "playSongPreview",
            "click .albumSongAddPlaylist": "addSongToPlaylist",
            "click #addAlbumPlaylist": "addAlbumToPlaylist"
        },

        playlistMenuView: "",

        initialize: function (options) {
            _.bindAll(this, "render");
            var self = this;

            this.playlistMenuView = new PlaylistMenuView();

            self.collection.bind("sync", function () {
                self.render();

                if ($.cookie(Utilities.TOKEN_COOKIE)) {
                    self.playlistMenuView.collection.fetch();
                }
            });
        },

        render: function () {
            this.$el.html(this.template({
                albumSongs: this.collection.toJSON(),
                userLoggedIn: $.cookie(Utilities.TOKEN_COOKIE)
            }));

            this.playlistMenuView.$el = this.$("#playlistMenuContainer");
            this.delegateEvents();
        },

        playSongPreview: function (event) {
            var previewUrl = event.target.parentElement.attributes["data-id"].value;
            AudioPlayer.playTrack(new AudioTrack(previewUrl));
        },

        addSongToPlaylist: function (event) {
            var that = this;
            var addSongToPlayListCallbackAfterClick = function () {
                var selectedSong = that.collection.at($(event.target.parentElement).index()).attributes.result;
                if (that.playlistMenuView.selectedPlaylistId) {
                    that.addSelectedSongToSpecificPlaylist(selectedSong);
                }
                else if (that.playlistMenuView.createNewPlaylistSelection) {
                    that.createNoNamePlaylist("song", selectedSong);
                }
            };

            this.displayPlaylistMenu(event, addSongToPlayListCallbackAfterClick);
        },

        addAlbumToPlaylist: function (event) {
            var that = this;
            var addAlbumToPlaylistCallbackAfterClick = function () {
                if (that.playlistMenuView.selectedPlaylistId) {
                    that.addAlbumToSpecificPlaylist();
                }
                else if (that.playlistMenuView.createNewPlaylistSelection) {
                    that.createNoNamePlaylist("album");
                }
            };

            this.displayPlaylistMenu(event, addAlbumToPlaylistCallbackAfterClick);
        },

        displayPlaylistMenu: function (event, callback) {
            var menu = $("#playlistMenuContainer");

            menu.removeClass("hidden");
            menu.css("left", event.pageX);
            menu.css("top", event.pageY + $("#albumContainer").scrollTop() - 80);

            event.stopPropagation();

            var that = this;
            var callbackOnMenuExit = function () {
                callback();

                that.playlistMenuView.resetMenu();
                $("#playlistMenuContainer").addClass("hidden");
                $("html").unbind("click", callbackOnMenuExit);
            };

            $("html").on("click", callbackOnMenuExit);
        },

        createNoNamePlaylist: function (itemToAdd, song) {
            var that = this;

            $.ajax(Config.url.securedApiUrl + "/tokenInfo", {
                type: "GET"
            }).done(function (owner) {
                that.playlistMenuView.collection.create({
                        "name": "No Name",
                        "owner": owner
                    },
                    {
                        type: "POST",
                        url: that.playlistMenuView.collection.url,

                        success: function (playlist) {
                            that.playlistMenuView.selectedPlaylistId = playlist.id;
                            that.playlistMenuView.render();
                            if (itemToAdd === "album") {
                                that.addAlbumToSpecificPlaylist();
                            }
                            else {
                                that.addSelectedSongToSpecificPlaylist(song)
                            }
                        }
                    });
            });


        },

        addSelectedSongToSpecificPlaylist: function (songData) {
            $.ajax({
                type: "POST",
                url: this.playlistMenuView.collection.url + "/" +
                this.playlistMenuView.selectedPlaylistId + "/tracks",
                data: songData,
                dataType: "json",

                success: function (data) {
                },

                error: function (data) {
                }
            });
        },

        addAlbumToSpecificPlaylist: function () {
            this.collection.models.forEach(function (song) {
                this.addSelectedSongToSpecificPlaylist(song.attributes.result);
            }, this);
        }
    });

    return AlbumSongsView;
});