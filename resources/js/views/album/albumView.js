/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "js/utils",
    "js/audio/AudioPlayer",
    "js/collections/songCollection",
    "js/views/album/albumSongsView",
    "text!templates/album/albumTemplate.html",
    "css!style/album"
], function ($, _, Backbone, Utilities, AudioPlayer, AlbumSongsCollection, AlbumSongsView, albumTemplate) {
    var AlbumView = Backbone.View.extend({
        template: _.template(albumTemplate),
        el: "#tabContainer",
        albumSongsView: "",

        events: {
            "click .playOverlay": "addAlbumToPlayer",
            "click .playAlbum": "addAlbumToPlayer"
        },

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            this.initializeAlbumSongsView();

            self.model.bind("sync", function () {
                self.render();
                self.albumSongsView.collection.fetch();
            });
        },

        initializeAlbumSongsView: function () {
            var songCollection = new AlbumSongsCollection();
            songCollection.url = this.model.url + "/tracks";

            this.albumSongsView = new AlbumSongsView({
                collection: songCollection
            });
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        render: function () {
            this.$el.html(this.template({
                albumInfo: this.model.toJSON()
            }));

            this.albumSongsView.$el = this.$("#albumSongsContainer");
            this.delegateEvents();
        },

        addAlbumToPlayer: function () {
            var playlist = this.albumSongsView.collection.getAllTracks();
            AudioPlayer.replaceList(playlist);
            AudioPlayer.play();
        }
    });

    return AlbumView;
});
