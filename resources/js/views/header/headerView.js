/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/models/userModel",
    "js/collections/searchCollection",
    "js/collections/userCollection",
    "js/views/header/searchResultsView",
    "js/views/header/loginView",
    "js/views/header/signUpView",
    "js/views/header/userView",
    "js/views/header/connectionView",
    "text!templates/header/headerTemplate.html",
    "css!style/search"
], function ($, _, Backbone, Config, Utilities, UserModel, SearchCollection, UserCollection, SearchResultsView,
             LoginView, SignUpView, UserView, ConnectionView, HeaderTemplate) {
    var HeaderView = Backbone.View.extend({
        template: _.template(HeaderTemplate),
        events: {
            "click #signUpButton": "showSignUpForm",
            "click #signUpFormClose": "hideSignUpForm",
            "click #loginButton": "showLoginForm",
            "click #loginFormClose": "hideLoginForm",
            "click .resultLink": "clearSearchBar",
            "keydown #searchBar": "searchBarKeyboardShortcut",
            "input #searchBar": "updateSearchResult"
        },
        el: "#header",
        searchResultView: "",
        signUpView: "",
        loginView: "",
        connectionView: "",
        userContainerView: "",

        initialiseSearchBar: function () {
            this.collection = new SearchCollection();
            this.userSearchCollection = new UserCollection({limit: 2});

            this.searchResultsView = new SearchResultsView({
                collection: this.collection,
                userSearchCollection: this.userSearchCollection
            });
        },

        initialiseViewWithUser: function () {
            var self = this;

            this.connectionView = new ConnectionView();

            this.signUpView = new SignUpView({
                model: this.user
            });

            this.loginView = new LoginView({
                model: this.user
            });

            this.userContainerView = new UserView({
                model: this.user
            });

            self.user.bind("sync change:token", function () {
                self.render();
            });
        },

        initialize: function (callback) {
            _.bindAll(this, "render");
            this.initializeUser(callback);
        },

        render: function () {
            this.$el.html(this.template());


            if (this.user.has("token") == true) {
                this.userContainerView.setElement(this.$("#userContainer")).render();
            } else {
                this.connectionView.setElement(this.$("#connectionButtons")).render();
                this.signUpView.setElement(this.$("#signUpFormContainer")).render();
                this.loginView.setElement(this.$("#loginFormContainer")).render();
            }

            this.searchResultsView.setElement(this.$("#searchResults")).render();

            this.clearSearchBar();

            return this;
        },

        searchBarKeyboardShortcut: function (event) {
            if (event.keyCode == Utilities.keyCodes.ENTER && !this.selectedClassExist()) {
                if ($("#searchBar").val()) {
                    window.location.href = "#research/" + encodeURIComponent($("#searchBar").val());
                    this.clearSearchBar();
                }
                event.preventDefault();
                return false;
            } else {
                this.enterKeyPress(event);
            }
        },

        updateSearchResult: function (event) {
            if (event.keyCode != Utilities.keyCodes.ARROW_DOWN && event.keyCode != Utilities.keyCodes.ARROW_UP) {
                this.updateResults();
            }
        },

        showSignUpForm: function (event) {
            $("#signUpFormContainer").css({
                "max-height": "500px"
            });
            this.hideLoginForm();
        },

        showLoginForm: function (event) {
            $("#loginFormContainer").css({
                "max-height": "500px"
            });
            this.hideSignUpForm();
        },

        hideSignUpForm: function (event) {
            $("#signUpFormContainer").css({
                "max-height": "0"
            });
        },

        hideLoginForm: function (event) {
            $("#loginFormContainer").css({
                "max-height": "0"
            });
        },


        modifySelectedClass: function (searchResults) {
            searchResults.removeClass("selected");
            $(".singleResult[data-resultnumber=" + index + "]").addClass("selected");
        },

        arrowsUpDown: function (keycode) {
            var searchResults = $(".singleResult");
            var numberOfResults = searchResults.length;

            if (keycode == Utilities.keyCodes.ARROW_DOWN) {
                if (!this.selectedClassExist()) {
                    searchResults.first().addClass("selected");
                } else {
                    this.incrementSelectedClass(numberOfResults, searchResults);
                }
            } else if (keycode == Utilities.keyCodes.ARROW_UP) {
                this.decrementSelectedClass(searchResults);
            }
        },

        enterKeyPress: function (event) {
            if (event.keyCode == Utilities.keyCodes.ENTER) {
                this.navigateToSelection();
                this.clearSearchBar();
            }
            else if (event.keyCode == Utilities.keyCodes.ARROW_DOWN || event.keyCode == Utilities.keyCodes.ARROW_UP) {
                this.arrowsUpDown(event.keyCode);
            }
            else {
                $("#searchResults").show();
            }
        },

        clearSearchBar: function () {
            $("#searchBar").val("");
            if (document.getElementById("#searchResults") !== null) {
                document.getElementById("#searchResults").innerHTML = "";
            }
            $("#searchResults").hide();
        },

        navigateToSelection: function () {
            var index = $(".singleResult").filter(".selected").data("resultnumber");
            var clickedElement = $(".singleResult[data-resultnumber=" + index + "]");
            var href = clickedElement.parents(".resultLink").attr("href");
            window.location.href = href;
        },

        selectedClassExist: function () {
            return $(".singleResult").filter(".selected").length > 0
        },

        incrementSelectedClass: function (numberOfResults, searchResults) {
            index = $(".singleResult").filter(".selected").data("resultnumber");

            if (index < numberOfResults) {
                index = index + 1;
                this.modifySelectedClass(searchResults);
            }
        },

        decrementSelectedClass: function (searchResults) {
            index = $(".singleResult").filter(".selected").data("resultnumber");

            if (index > 0) {
                index = index - 1;
                this.modifySelectedClass(searchResults)
            }
        },

        updateResults: function () {
            this.collection.url = Config.url.apiUrl + "/search?q=" + encodeURIComponent($("#searchBar").val()) + "&limit=5";
            this.collection.fetch();

            if ($.cookie(Utilities.TOKEN_COOKIE)) {
                this.userSearchCollection.url = Config.url.securedApiUrl + "/search/users?q=" + encodeURIComponent($("#searchBar").val());
                this.userSearchCollection.fetch();
            } else {
                this.userSearchCollection.reset()
            }
        },

        initializeUser: function (callback) {
            this.user = new UserModel("/login");
            if ($.cookie(Utilities.TOKEN_COOKIE)) { //sert seulement pour la performance
                var that = this;
                console.log("try to login")
                this.user.fetch({
                        url: Config.url.securedApiUrl + "/tokenInfo",
                        headers: {
                            Authorization: $.cookie(Utilities.TOKEN_COOKIE)
                        }
                    }
                ).done(function () {
                        $.ajaxSetup({
                            headers: {"Authorization": $.cookie(Utilities.TOKEN_COOKIE)}
                        });
                    }).fail(function () {
                        that.user.deleteUserCookies();
                    }).always(function () {
                        that.initialiseSearchBar();
                        that.initialiseViewWithUser();
                        that.render();
                        callback();
                    });
                this.user.url = Config.url.securedApiUrl + "/login";
            } else {
                this.initialiseSearchBar();
                this.initialiseViewWithUser();
                this.render();
                callback();
            }
        }
    });

    $(document).click(function (e) {
        //TODO show or hide searchResults if the user click somewhere on page
        var target = e.target;

        if ($(target).parents("#musicSearch").length >= 1 && $("#searchBar").val() !== "") {
            $("#searchResults").show();
        } else {
            $("#searchResults").hide();
        }
        if ($(target).parents("#searchResults").length > 0) {
            $("#searchResults").hide();
        }
    });

    return HeaderView;
});
