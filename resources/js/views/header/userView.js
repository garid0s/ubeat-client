define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/gravatar",
    "js/models/userModel",
    "text!templates/header/userTemplate.html",
    "css!style/static.css"
], function ($, _, Backbone, Config, Utilities, Gravatar, UserModel, userTemplate) {
    var UserView = Backbone.View.extend({
        template: _.template(userTemplate),
        events: {
            "click #disconnection": "disconnection"
        },
        el: "#userContainer",

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;
        },
        render: function () {
            this.$el.html(this.template({
                userInfo: this.model.toJSON()
            }));
        },
        disconnection: function () {
            this.model.fetch({url: Config.url.securedApiUrl + "/logout"});
            this.model.deleteUserCookies();
            this.model.clear();

            window.location.href = "#";
        }
    });

    return UserView;
});