define([
    "jquery",
    "underscore",
    "backbone",
    "text!templates/header/searchResultTemplate.html",
    "css!style/static.css"
], function ($, _, Backbone, SearchResultsTemplate) {
    var SearchResultsView = Backbone.View.extend({
        template: _.template(SearchResultsTemplate),
        el: "#searchResults",

        userSearchCollection: "",

        initialize: function (options) {
            _.bindAll(this, "render");
            var self = this;
            self.collection.bind("sync", function () {
                self.render();
            });
            this.userSearchCollection = options.userSearchCollection
        },

        render: function () {

            if (this.collection.length != 0 || !$("#searchBar").val()) {
                this.$el.html(this.template({
                    headerInfo: this.collection.toJSON(),
                    userInfo: this.userSearchCollection.toJSON()
                }));
            }
        }
    });

    return SearchResultsView;
});