define([
    "jquery",
    "underscore",
    "backbone",
    "js/models/userModel",
    "text!templates/header/signUpTemplate.html",
    "js/validators/signUpValidator",
    "css!style/login.css",
    "css!style/static.css"
], function ($, _, Backbone, UserModel, SignUpTemplate, SignUpValidator) {
    var SignUpView = Backbone.View.extend({
        template: _.template(SignUpTemplate),
        el: "#signUpFormContainer",
        email: "",
        password: "",

        events: {
            "click #signUpFormOk": "signUp",
            "keyup #signUpEmail": "validateEmail"
        },
        initialize: function () {
            _.bindAll(this, "render");
            var self = this;
            this.signUpModel = new UserModel("/signup");

            self.signUpModel.bind("sync", function () {
                self.model.save({
                    "email": self.email,
                    "password": self.password
                }, {
                    success: function () {
                        self.model.saveInCookies();
                    },

                    error: function (model, response) {
                        model.clear()
                    }
                });
            });
        },

        signUp: function (event) {
            this.clearErrors();

            if (this.modelIsValid()) {
                this.email = $("#signUpEmail").val();
                this.password = $("#signUpPasswordField").val();

                var that = this;
                this.signUpModel.save({
                    "email": this.email,
                    "name": $("#signUpName").val(),
                    "password": this.password
                }, {
                    error: function (model, response) {
                        if (response.status === 401) {
                            $("#emailAlreadyExist").text("Email already exists!");
                            $("#signUpEmail").val("");
                            $("#signUpPasswordField").val("");
                            $("#signUpConfirmPasswordField").val("");
                        }
                        else {
                            $("#emailAlreadyExist").text("Service not available!");
                            $("#signUpEmail").val("");
                            $("#signUpPasswordField").val("");
                            $("#signUpConfirmPasswordField").val("");
                        }
                    }
                });
            }
        },

        modelIsValid: function () {
            var attributes = {
                "email": $("#signUpEmail").val(),
                "name": $("#signUpName").val(),
                "password": $("#signUpPasswordField").val(),
                "confirmPassword": $("#signUpConfirmPasswordField").val()
            };

            var errors = SignUpValidator(attributes);
            if (errors) {
                if (errors.email) {
                    $("#signUpEmail").css({"background-color": "#FF6347"});
                    $("#signUpEmailError").text(errors.email);
                }

                if (errors.name) {
                    $("#signUpName").css({"background-color": "#FF6347"});
                    $("#signUpNameError").text(errors.name);
                }

                if (errors.password) {
                    $("#signUpPasswordField").css({"background-color": "#FF6347"});
                    $("#signUpPasswordError").text(errors.password);
                }
                return false;
            }
            else {
                return true;
            }

        },

        clearErrors: function () {
            $("#signUpName").css({"background-color": "white"});
            $("#signUpEmail").css({"background-color": "white"});
            $("#signUpPasswordField").css({"background-color": "white"});
            $("#signUpEmailError").text("");
            $("#signUpNameError").text("");
            $("#signUpPasswordError").text("");
            $("#emailAlreadyExist").text("");
        },

        render: function () {
            this.$el.html(this.template({}));
        }
    });

    return SignUpView;
});