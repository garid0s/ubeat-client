define([
    "jquery",
    "underscore",
    "backbone",
    "text!templates/header/connectionTemplate.html",
    "css!style/static.css"
], function ($, _, Backbone, userTemplate) {
    var UserView = Backbone.View.extend({
        template: _.template(userTemplate),
        el: "#connectionButtons",

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;
        },

        render: function () {
            this.$el.html(this.template());
        }
    });

    return UserView;
});