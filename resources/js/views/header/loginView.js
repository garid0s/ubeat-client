define([
    "jquery",
    "underscore",
    "backbone",
    "js/models/userModel",
    "text!templates/header/loginTemplate.html",
    "js/validators/loginValidator",
    "js/utils",
    "css!style/login.css",
    "css!style/static.css"
], function ($, _, Backbone, UserModel, loginTemplate, LoginValidator, Utilities) {
    var SignUpView = Backbone.View.extend({
        template: _.template(loginTemplate),
        el: "#loginFormContainer",

        events: {
            "click #loginFormOk": "login"
        },

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;
            this.render();
        },

        login: function (event) {
            this.clearErrors();
            if (this.modelIsValid()) {
                var self = this;
                this.model.save({ //refact possible avec form ? p-e un moyen plus clean
                    "email": $("#loginEmail").val(),
                    "password": $("#loginPasswordField").val()
                }, {

                    success: function () {
                        self.model.saveInCookies();
                    },

                    error: function (model, response) {
                        $("#loginPasswordField").val("");
                        $("#loginError").text("Bad username or password!");
                    }
                });
            }
        },

        render: function () {
            this.$el.html(this.template({
                userInfo: this.model.toJSON()
            }));
        },

        clearErrors: function () {
            $("#loginError").text("");
            $("#loginPasswordError").text("");
            $("#loginEmailError").text("");
            $("#loginEmail").css({"background-color": "white"});
            $("#loginPasswordField").css({"background-color": "white"});
        },


        modelIsValid: function () {
            var attributes = {
                "email": $("#loginEmail").val(),
                "password": $("#loginPasswordField").val()
            };

            var errors = LoginValidator(attributes);
            if (errors) {
                if (errors.email) {
                    $("#loginEmail").css({"background-color": "#FF6347"});
                    $("#loginEmailError").text(errors.email);
                }

                if (errors.password) {
                    $("#loginPasswordField").css({"background-color": "#FF6347"});
                    $("#loginPasswordError").text(errors.password);
                }
                return false;
            }
            else {
                return true;
            }

        }
    });

    return SignUpView;
});