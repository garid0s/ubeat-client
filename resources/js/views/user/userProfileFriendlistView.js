define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/models/userModel",
    "text!templates/user/userProfileFriendlistTemplate.html",
    "css!style/userProfile"

], function ($, _, Backbone, Config, Utilities, UserModel, UserProfileFriendlistTemplate) {
    var UserProfileFriendlistView = Backbone.View.extend({
        template: _.template(UserProfileFriendlistTemplate),
        el: "#userProfileFriendlist",

        events: {
            "click #unfollowFollowedUser": "onUnfollowClick"
        },

        initialize: function () {
            var self = this;
            _.bindAll(this, "render");
            self.model.bind("sync", function () {
                self.render();
            })
        },

        render: function () {
            var numberOfFriends = this.model.get("following").length;
            var isConnectedUser;
            ($.cookie(Utilities.USER_ID) === this.model.get("userId")) ? isConnectedUser = true : isConnectedUser = false;
            this.$el.html(this.template({
                user: this.model,
                numberOfFriends: numberOfFriends,
                isConnectedUser: isConnectedUser
            }));
            this.delegateEvents();
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        onUnfollowClick: function (event) {
            var self = this;
            var id = $(event.target).data("id");
            var model = new UserModel("/users/" + id);
            model.fetch().done(function () {
                model.unfollowUser(function () {
                    self.model.fetch();
                });
            });
        }
    });
    return UserProfileFriendlistView;
});
