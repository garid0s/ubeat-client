define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/models/userModel",
    "text!templates/user/userProfileDescriptionTemplate.html",
    "css!style/userFriends.css"
], function ($, _, Backbone, Config, Utilities, UserModel, UserProfileDescriptionTemplate) {
    var UserProfileDescriptionView = Backbone.View.extend({
        template: _.template(UserProfileDescriptionTemplate),
        el: ".topUserPage",
        followingUser: false,
        connectedUser: "",

        events: {
            "click #toggleFollowUser": "onFollowUserClick"
        },

        initialize: function () {
            var self = this;
            _.bindAll(this, "render");
            self.model.bind("sync", function () {
                var connectedUserId = $.cookie("user-id");
                self.connectedUser = new UserModel("/users/" + connectedUserId);
                self.connectedUser.fetch().done(self.render);
            });
        },

        render: function () {
            this.updateFollowingUser();
            this.$el.html(this.template({
                userOnPage: this.model.toJSON(),
                followingUser: this.followingUser
            }));
            var connectedUserId = $.cookie(Utilities.USER_ID);
            var pageUserId = this.model.get("userId");
            if (connectedUserId && connectedUserId === pageUserId) {
                $("#toggleFollowUser").hide();
            }

            this.delegateEvents();
        },

        updateFollowingUser: function () {
            var userOnPageId = this.model.get("userId");
            var self = this;
            _.each(this.connectedUser.get("following"), function (f) {
                if (f["id"] === userOnPageId) {
                    self.followingUser = true;
                }
            });
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        onFollowUserClick: function (event) {
            event.stopPropagation();
            var self = this;
            if (self.followingUser) {
                self.model.unfollowUser(function () {
                    $(event.target).text("+Follow");
                    self.followingUser = false;
                });
            }
            else {
                self.model.followUser(function () {
                    $(event.target).text("Unfollow");
                    self.followingUser = true;
                });
            }
        }

    });

    return UserProfileDescriptionView;
});
