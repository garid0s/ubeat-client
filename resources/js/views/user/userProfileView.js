define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils",
    "js/models/userModel",
    "js/collections/playlistCollection",
    "js/views/playlist/allPlaylistView",
    "text!templates/user/userProfileTemplate.html",
    "js/views/user/userProfileFriendlistView",
    "js/views/user/userProfileDescriptionView",
    "css!style/userProfile"

], function ($, _, Backbone, Config, Utilities, UserModel, PlaylistCollection, AllPlaylistView,
             UserProfileTemplate, UserProfileFriendlistView, UserProfileDescriptionView) {
    var UserProfileView = Backbone.View.extend({
        template: _.template(UserProfileTemplate),
        el: "#tabContainer",
        allPlaylistView: "",

        initialize: function () {
            var self = this;
            _.bindAll(this, "render");
            self.render();
            self.allPlaylistView = new AllPlaylistView({
                collection: self.collection,
                editable: true
            });
            self.userProfileFriendlistView = new UserProfileFriendlistView({model: self.model});
            self.userProfileDescriptionView = new UserProfileDescriptionView({model: self.model});
        },

        render: function () {
            this.$el.html(this.template());
        }
    });

    return UserProfileView;
});
