define([
    "jquery",
    "underscore",
    "backbone",
    "js/audio/AudioTrack",
    "js/audio/AudioPlayer",
    "text!templates/user/userProfilePlaylistSongsTemplate.html",
    "text!templates/playlist/playlistSongsTemplate.html"
], function ($, _, Backbone, AudioTrack, AudioPlayer, UserProfilePlaylistSongsTemplate, playlistSongsTemplate) {
    var PlaylistSongsView = Backbone.View.extend({
        template: _.template(playlistSongsTemplate),
        events: {
            "click .deletePlaylistTrackButton": "deleteTrack",
            "click .playlistSongEntry": "playSongPreview"
        },

        initialize: function (options) {
            _.bindAll(this, "render");
            var self = this;
            self.options = options
            self.setTemplate();
            self.collection.bind("sync add remove", function () {
                self.render();
            });
        },

        setTemplate: function () {
            if (this.options.editable) {
                this.template = _.template(UserProfilePlaylistSongsTemplate);
            }
        },

        render: function () {
            this.$el.html(this.template({
                playlistSongs: this.collection.toJSON()
            }));
            this.delegateEvents();
        },

        deleteTrack: function (event) {
            var id = $(event.target).data("id");
            var track = this.collection.get(id);
            track.url = this.collection.url + "/tracks/" + track.attributes.songId;
            this.fixNumberOfSongs(event);
            track.destroy();
            this.collection.remove(track);

            event.stopPropagation();
        },

        fixNumberOfSongs: function (event) {
            var playlistContainer = $(event.target).parents(".playlistContainer");
            var playlistNumberOfSongs = $(playlistContainer).find(".playlistNumberOfSongs");
            var numberOfSongs = parseInt(playlistNumberOfSongs.html()) - 1;
            playlistNumberOfSongs.html(numberOfSongs.toString());
            if (numberOfSongs <= 1) {
                $(playlistNumberOfSongs).siblings(".playlistNumberOfSongsLabel").html("song");
            }
        },

        playSongPreview: function (event) {
            var container = event.target.attributes["data-id"];
            var previewUrl;

            if (typeof container !== "undefined") {
                previewUrl = container.value;
            }
            else {
                previewUrl = $(event.target).parents(".playlistSongEntry")[0].attributes["data-id"].value;
            }

            AudioPlayer.playTrack(new AudioTrack(previewUrl));
        }

    });

    return PlaylistSongsView;
});