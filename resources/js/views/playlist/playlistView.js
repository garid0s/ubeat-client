/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/audio/AudioTrack",
    "js/audio/AudioPlayer",
    "js/collections/songCollection",
    "js/views/playlist/playlistSongsView",
    "text!templates/user/userProfilePlaylistTemplate.html",
    "text!templates/playlist/playlistTemplate.html",
    "css!style/playlist"
], function ($, _, Backbone, Config, AudioTrack, AudioPlayer, PlaylistSongsCollection, PlaylistSongsView,
             UserProfilePlaylistTemplate, playlistTemplate) {
    var playlistView = Backbone.View.extend({
        template: _.template(playlistTemplate),
        playlistSongsView: "",
        playlistNumber: "",
        isOpen: false,
        songsLoaded: false,

        events: {
            "click .playlistInfo": "togglePlaylist",
            "click .playlistNumber": "playAllPlaylistSongs"
        },

        initialize: function (options) {
            _.bindAll(this, "render");
            var self = this;
            this.options = options;
            self.setTemplate();
            this.initializePlaylistSongsView();

            self.model.bind("sync", function () {
                self.render();
            });

        },

        setTemplate: function () {
            if (this.options.editable) {
                this.template = _.template(UserProfilePlaylistTemplate);
            }
        },

        initializePlaylistSongsView: function () {
            var songsCollection = new PlaylistSongsCollection();
            songsCollection.url = this.model.url;

            this.playlistSongsView = new PlaylistSongsView({
                collection: songsCollection,
                editable: this.options.editable
            })
        },

        render: function () {
            this.$el.html(this.template({
                playlist: this.model.toJSON(),
                playlistNumber: this.playlistNumber
            }));
            this.playlistSongsView.$el = this.$(".playlistSongs");
            return this;
        },

        togglePlaylist: function (event) {
            if (this.eventIsInToggleZone(event)) {
                var playlistNumber = this.getPlaylistNumber(event);
                event.target = $(event.target).parents("#playlistContainer" + playlistNumber.toString());
                var playlistSongs = $(event.target).children().children(".playlistSongs");
                if (this.isOpen) {
                    playlistSongs.addClass("hidden");
                    this.isOpen = false;
                }
                else {
                    this.loadSongs();
                    playlistSongs.removeClass("hidden");
                    this.isOpen = true;
                }

            }
        },

        eventIsInToggleZone: function (event) {
            if (!$(event.target).parents().hasClass("playlistSongs") && !$(event.target).parents().hasClass("playlistActions") && !$(event.target).parents().hasClass("editPlaylist")
                && !$(event.target).hasClass("playlistNumber")) {
                return true;
            }
            return false;
        },

        getPlaylistNumber: function (event) {
            var playlistContainer = $(event.target).parents(".playlistContainer")[0];
            var number = $(playlistContainer).find(".playlistNumber").html();
            return parseInt(number);
        },

        playAllPlaylistSongs: function (event) {
            var that = this;
            if (!this.songsLoaded) {
                this.playlistSongsView.collection.fetch().done(function () {
                    that.launchAudioPlayer();
                });
                this.songsLoaded = true;
            }
            else {
                that.launchAudioPlayer();
            }
        },

        loadSongs: function () {
            if (!this.songsLoaded) {
                this.playlistSongsView.collection.fetch();
                this.songsLoaded = true;
            }
        },

        launchAudioPlayer: function () {
            AudioPlayer.replaceList(this.playlistSongsView.collection.getAllTracks());
            AudioPlayer.play()
        }

    });

    return playlistView;
});
