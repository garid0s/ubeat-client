/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/utils.js",
    "js/models/playlistModel",
    "js/collections/playlistCollection",
    "js/collections/songCollection.js",
    "js/views/playlist/playlistView",
    "text!templates/playlist/allPlaylistsTemplate.html",
    "text!templates/user/userProfileAllPlaylistsTemplate.html",
    "css!style/playlist"
], function ($, _, Backbone, Config, Utilities, PlaylistModel, PlaylistCollection, PlaylistSongsCollection,
             PlaylistView, allPlaylistTemplate, UserProfileAllPlaylistsTemplate) {
    var allPlaylistView = Backbone.View.extend({
        template: _.template(allPlaylistTemplate),
        el: "#tabContainer",
        playlistView: "",

        events: {
            "click #newPlaylistButton": "createNewPlaylist",
            "click .deletePlaylistButton": "deletePlaylist",
            "click .editPlaylistButton": "editPlaylist",
            "click .cancelButton": "endEditPlaylist",
            "click .okEditPlaylistButton": "acceptEditPlaylist",
            "keyup #newPlaylistName": "onKeyUpCreateNewPlaylist",
            "keyup .editPlaylistName": "onKeyUPEditPlaylist"
        },

        initialize: function (options) {
            _.bindAll(this, "render");
            var self = this;
            self.options = options;
            self.setTemplate();

            self.collection.bind("sync remove", function () {
                self.render();
            });

            if ($.cookie(Utilities.TOKEN_COOKIE)) {
                self.collection.fetch()
            }
            else {
                self.render();
            }
        },

        setTemplate: function () {
            if (this.options.editable) {
                this.template = _.template(UserProfileAllPlaylistsTemplate);
                this.setElement("#userProfilePlaylists");
            }
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        render: function () {
            this.$el.html(this.template({
                playlists: this.collection.toJSON()
            }));
            this.renderAllPlaylist();
            this.delegateEvents();
        },

        createNewPlaylist: function (event) {
            var newPlaylistName = $("#newPlaylistName").val();
            var that = this;
            $.ajax(Config.url.securedApiUrl + "/tokenInfo", {
                type: "GET",
                error: function (event) {
                    $(".playlistErrorMessage").text("You must be logged in to create a new playlist!");
                    $(".playlistErrorMessage").removeClass("hidden");
                },
                success: function () {
                    that.validatePlaylistName(newPlaylistName);
                }
            }).done(function (owner) {
                that.collection.create({
                        "name": newPlaylistName,
                        "owner": owner
                    },
                    {
                        type: "POST",
                        url: that.collection.url,
                        validate: true
                    });
            });

            $("#newPlaylistName").val("");
        },

        deletePlaylist: function (event) {
            var playlist = this.getSelectedPlaylist(event);
            playlist.destroy();
            this.collection.remove(playlist);
        },

        editPlaylist: function (event) {
            var editPlaylist = $(event.target).parent().siblings(".playlistInfo").children(".editPlaylist");
            var playlistName = $(event.target).parent().siblings(".playlistInfo").children(".playlistName");
            editPlaylist.children(".editPlaylistName").val(playlistName.text().trim());
            editPlaylist.removeClass("hidden");
            playlistName.addClass("hidden");
        },

        endEditPlaylist: function (event) {
            var editPlaylist = $(event.target).parent();
            var playlistName = $(event.target).parent().siblings(".playlistName");
            editPlaylist.addClass("hidden");
            playlistName.removeClass("hidden");
        },

        acceptEditPlaylist: function (event) {
            var playlistName = $(event.target).siblings(".editPlaylistName").val().trim();
            var playlist = this.getSelectedPlaylist(event);

            var that = this;

            if (playlistName === "") {
                $(event.target).siblings(".editPlaylistName").attr("placeholder", "The name cannot be empty!");
            }
            else {
                playlist.fetch({
                    success: function () {
                        playlist.save({
                            "name": playlistName
                        }, {
                            validate: true
                        });

                        that.endEditPlaylist(event);
                    }
                });
            }
        },

        getSelectedPlaylist: function (event) {
            var id = $(event.target).data("id");
            var playlist = this.collection.get(id);
            return playlist
        },

        setSelectedPlaylist: function (event) {
            var id = $(event.target).data("id");
            var playlistModel = new PlaylistModel({id: id});
            this.playlistView = new PlaylistView({
                model: playlistModel
            });
        },

        renderAllPlaylist: function () {
            var playlistViews = this.getAllPlaylistViews();
            for (var i = 0; i < playlistViews.length; i++) {
                var playlistNumber = i + 1;
                this.$("#playlistContainer" + playlistNumber.toString()).html(playlistViews[i].render().el);
                playlistViews[i].playlistNumber = playlistNumber;
                playlistViews[i].model.fetch();
            }
        },

        getPlaylistView: function (playlist) {
            var playlistModel = new PlaylistModel({id: playlist.id});
            var playlistView = new PlaylistView({
                model: playlistModel,
                editable: this.options.editable
            });

            return playlistView;
        },

        getAllPlaylistViews: function () {
            var playlistViews = [];
            for (var i = 0; i < this.collection.length; i++) {
                playlistViews.push(this.getPlaylistView(this.collection.at(i)));
            }
            return playlistViews;
        },

        validatePlaylistName: function (name) {
            if (name === "") {
                $(".playlistErrorMessage").text("The name cannot be empty!");
                $(".playlistErrorMessage").removeClass("hidden");
                return false;
            } else {
                $(".playlistErrorMessage").addClass("hidden");
                return true;
            }
        },

        onKeyUpCreateNewPlaylist: function (event) {
            var that = this;
            if (event.keyCode === Utilities.keyCodes.ENTER) {
                that.createNewPlaylist(event)
            }
        },

        onKeyUPEditPlaylist: function (event) {
            var that = this;
            event.target = $(event.target).siblings(".okEditPlaylistButton")[0];

            if (event.keyCode === Utilities.keyCodes.ENTER) {
                that.acceptEditPlaylist(event);
            }
            else if (event.keyCode === Utilities.keyCodes.ESCAPE) {
                that.endEditPlaylist(event);
            }
        }

    });

    return allPlaylistView;
});
