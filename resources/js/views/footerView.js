/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "js/utils",
    "js/audio/AudioPlayer",
    "text!templates/footerTemplate.html"
], function ($, _, Backbone, Utilities, AudioPlayer, FooterTemplate) {
    var FooterView = Backbone.View.extend({
        template: _.template(FooterTemplate),
        el: "#currentSongPlayer",

        events: {
            "input #mainVolume": "setPlayerVolume",
            "click .searchIcon": "activateMobileSearchBar",
            "click #mobileSearchBarInput": "onClickOfMobileSearchBar",
            "keyup #mobileSearchBarInput": "onKeyUpOfMobileSearchBar"
        },

        initialize: function () {
            _.bindAll(this, "render");
            this.render();
        },
        render: function () {
            this.$el.html(this.template());

            $("#mainVolume").val(AudioPlayer.volume * 100);
        },

        setPlayerVolume: function (volume) {
            AudioPlayer.setVolume(volume.currentTarget.value / 100);
            this.adjustVolumeLevelFromValue(volume.currentTarget.value);
        },

        adjustVolumeLevelFromValue: function (value) {
            var volume = $(".volumeLevel");

            if (value <= 5) {
                volume.css("background-position", "0 0");
            } else if (value <= 25) {
                volume.css("background-position", "0 -25px");
            } else if (value <= 75) {
                volume.css("background-position", "0 -50px");
            } else {
                volume.css("background-position", "0 -75px");
            }
        },

        activateMobileSearchBar: function (event) {
            $(".mobileSearchBar").show();
            $("#mobileSearchBarInput").focus();

            event.stopPropagation();

            var hideMobileSearchBar = function () {
                $(".mobileSearchBar").hide();
                $("html").unbind("click", hideMobileSearchBar);
            };

            $("html").bind("click", hideMobileSearchBar);
        },

        onClickOfMobileSearchBar: function (event) {
            event.stopPropagation();
        },

        onKeyUpOfMobileSearchBar: function (event) {
            if (event.keyCode == Utilities.keyCodes.ENTER) {
                window.location.href = "#research/" + encodeURIComponent($("#mobileSearchBarInput").val());
                $(".mobileSearchBar").hide();
                $("#mobileSearchBarInput").val("");
            }
        }
    });

    return FooterView;
});