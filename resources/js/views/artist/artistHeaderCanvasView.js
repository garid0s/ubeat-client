/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "js/collections/albumCollection",
    "js/views/artist/albumMiniatureView"
], function ($, _, Backbone, AlbumCollection) {
    var ArtistHeaderCanvasView = Backbone.View.extend({
        albumCoverImages: [],

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            self.collection.bind("sync", function () {
                self.loadImagesAndRender();
            });
        },

        cleanup: function () {
            $(window).off("resize");
        },

        loadImagesAndRender: function () {
            var nbrOfImages = this.collection.length;
            var loadedCount = 0;

            var self = this;
            var checkAllLoaded = function () {
                loadedCount++;
                if (loadedCount == nbrOfImages) {
                    self.render();
                }
            };

            for (var i = 0; i < nbrOfImages; i++) {
                this.albumCoverImages[i] = new Image();
                this.albumCoverImages[i].onload = checkAllLoaded;
                this.albumCoverImages[i].onerror = checkAllLoaded;
                this.albumCoverImages[i].src = this.collection.at(i).get("albumCoverImageLowResUrl");
            }

            $(window).on("resize", this.render.bind(this));
        },

        render: function () {
            var canvasId = "artistHeaderCanvas";
            var size = 100;

            if ($("#" + canvasId).length) {
                var canvas = document.getElementById(canvasId);
                var context = canvas.getContext("2d");
                context.canvas.width = canvas.offsetWidth;
                context.canvas.height = canvas.offsetHeight;


                var nbrOfAlbums = this.collection.length;
                var imageIndex = 0;

                for (var x = 0; x < context.canvas.width; x += size) {
                    for (var y = 0; y < context.canvas.height; y += size) {
                        context.drawImage(this.albumCoverImages[imageIndex], x, y, size, size);
                        imageIndex = (imageIndex + 1) % nbrOfAlbums;
                    }
                }
            }
        }
    });

    return ArtistHeaderCanvasView;
});
