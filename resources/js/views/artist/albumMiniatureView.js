define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/audio/AudioPlayer",
    "js/collections/songCollection",
    "text!templates/artist/albumMiniatureTemplate.html"
], function ($, _, Backbone, Config, AudioPlayer, SongCollection, albumMiniatureTemplate) {
    var AlbumMiniatureView = Backbone.View.extend({
        template: _.template(albumMiniatureTemplate),

        events: {
            "click .playOverlay": "playSelectedAlbum"
        },

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            self.collection.bind("sync", function () {
                self.render();
            });
        },

        render: function () {
            this.$el.html(this.template({
                albumMiniatures: this.collection.toJSON()
            }));

            this.delegateEvents();
        },

        cleanup: function () {
            this.undelegateEvents();
        },

        playSelectedAlbum: function (event) {
            AudioPlayer.stop();

            var songCollection = new SongCollection();
            songCollection.url = Config.url.apiUrl + "/albums/" + $(event.target).data("id") + "/tracks";

            songCollection.fetch({
                success: function (data) {
                    var playlist = songCollection.getAllTracks();

                    AudioPlayer.replaceList(playlist);
                    AudioPlayer.play();
                }
            });
        }
    });

    return AlbumMiniatureView;
});
