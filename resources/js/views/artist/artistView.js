/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "config",
    "js/collections/albumCollection",
    "./albumMiniatureView",
    "./artistHeaderCanvasView",
    "text!templates/artist/artistTemplate.html",
    "css!style/artist"
], function ($, _, Backbone, Config, AlbumCollection, AlbumMiniatureView, ArtistHeaderCanvasView, artistTemplate) {
    var ArtistView = Backbone.View.extend({
        template: _.template(artistTemplate),
        el: "#tabContainer",
        albumMiniatureView: "",
        artistHeaderCanvasView: "",

        initialize: function () {
            _.bindAll(this, "render");
            var self = this;

            this.initializeSubviews();

            self.model.bind("sync", function () {
                self.albumMiniatureView.collection.fetch();
                self.render();
            });
        },

        cleanup: function () {
            this.artistHeaderCanvasView.cleanup();
            this.$el.empty();
        },

        initializeSubviews: function () {
            var albumCollection = new AlbumCollection();
            albumCollection.url = this.model.url + "/albums";

            this.albumMiniatureView = new AlbumMiniatureView({
                collection: albumCollection
            });

            this.artistHeaderCanvasView = new ArtistHeaderCanvasView({
                collection: albumCollection
            });
        },

        render: function () {
            this.$el.html(this.template({
                artistInfo: this.model.toJSON()
            }));
            this.albumMiniatureView.$el = this.$("#artistAlbums");
        }

    });

    return ArtistView;
});
