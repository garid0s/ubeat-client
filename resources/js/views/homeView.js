/*global define*/
define([
    "jquery",
    "underscore",
    "backbone",
    "text!templates/homeTemplate.html",
    "css!style/home.css"

], function ($, _, Backbone, HomeTemplate) {
    var HomeView = Backbone.View.extend({
        template: _.template(HomeTemplate),
        el: "#tabContainer",

        initialize: function () {
            _.bindAll(this, "render");
        },

        render: function () {
            this.$el.html(this.template());
        }
    });

    return HomeView;
});
