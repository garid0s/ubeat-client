define(function () {
    var AudioTrack = function (trackUrl) {
        "use strict";
        this.audio = new Audio();
        this.itunesTrack = {};

        if (trackUrl !== undefined) {
            this.loadItunesTrack({"previewUrl": trackUrl});
        }
    };

    /*
     * This object is basically a wrapper/helper around the Javascript native Audio object
     */
    AudioTrack.prototype = {
        loadItunesTrack: function (itunesTrack, songUrlKey) {
            "use strict";
            this.stop();
            this.itunesTrack = itunesTrack;

            if (songUrlKey !== undefined) {
                this.audio.src = itunesTrack[songUrlKey];
            } else {
                this.audio.src = itunesTrack.previewUrl;
            }
        },

        getItunesParameter: function (param) {
            "use strict";
            if (typeof this.itunesTrack === "object") {
                param = this.itunesTrack[param];
            }

            return param;
        },

        play: function () {
            "use strict";
            this.audio.play();
        },

        pause: function () {
            "use strict";
            this.audio.pause();
        },

        stop: function () {
            "use strict";
            this.audio.pause();
            if (this.audio.currentTime > 0) {
                this.audio.currentTime = 0;
            }
        },

        // default value is 1 (highest)
        volume: function (volume) {
            "use strict";

            if (typeof volume != "undefined" && volume >= 0 && volume <= 1) {
                this.audio.volume = volume;
            }

            return this.audio.volume;
        },

        duration: function () {
            "use strict";
            return this.audio.duration;
        },

        muted: function () {
            "use strict";
            return this.audio.muted;
        },

        paused: function () {
            "use strict";
            return this.audio.paused;
        },

        toggleMuted: function () {
            "use strict";
            this.audio.muted = !this.audio.muted;
        },

        togglePlayPause: function () {
            "use strict";
            if (this.audio.paused) {
                this.audio.play();
            } else {
                this.audio.pause();
            }
        },

        currentTime: function (time) {
            "use strict";
            // Let the browser handle the exceptions and all

            if (time !== undefined) {
                this.audio.currentTime = time;
            }
            return this.audio.currentTime;
        },

        currentTimePercent: function (percent) {
            "use strict";
            if (percent !== undefined) {
                if (percent <= 1 && percent >= 0) {
                    var sec = this.audio.duration * percent;
                    this.currentTime(sec);
                } else {
                    throw {name: "Invalid Value", message: "The percentage of time must be between 0 and 1"};
                }
            }


            return this.audio.currentTime / this.audio.duration;
        },

        atBeginning: function () {
            return this.currentTimePercent() === 0;
        },

        canPlayThrough: function (callback) {
            "use strict";
            this.audio.oncanplaythrough = callback;
        },

        canPlay: function (callback) {
            "use strict";
            this.audio.oncanplay = callback;
        },

        readyState: function () {
            "use strict";
            return this.audio.readyState;
        },

        onPlaying: function (callback) {
            "use strict";
            this.audio.onplaying = callback;
        },

        onEnded: function (callback) {
            "use strict";
            this.audio.onended = callback;
        }
    };

    return AudioTrack;
});
