define([
    "js/audio/AudioTrack"
], function (AudioTrack) {
    var AudioPlaylist = function () {
        "use strict";

        this.array = [];
        this.totalTimeValue = 0;
    };

    AudioPlaylist.prototype = {
        length: function () {
            "use strict";
            return this.array.length;
        },

        appendItunesAlbum: function (album) {
            "use strict";
            if (typeof album !== "object") {
                throw {
                    name: "TypeError",
                    message: "The itunes album must be an array of itunes track",
                    invalidObj: album
                };
            }

            for (var i = 0; i < album.length; i++) {
                this.appendItunesTrack(album[i]);
            }
        },

        appendItunesTrack: function (track) {
            "use strict";
            if (typeof track != "object") {
                throw {
                    name: "TypeError",
                    message: "The track must be an itunes track",
                    invalidObj: track
                };
            }

            var newTrack = new AudioTrack();
            newTrack.loadItunesTrack(track);
            this.array.push(newTrack);
        },

        empty: function () {
            "use strict";
            this.array = [];
        },

        remove: function (index) {
            "use strict";
            var oldValue = this.array[index];
            this.array[index] = undefined;
            this.rebuildArray();

            return oldValue;
        },

        totalTime: function () {
            "use strict";
            var totalTime = 0;

            for (var i = this.array.length - 1; i >= 0; i--) {
                totalTime += this.array[i].duration();
            }
            return totalTime;
        },

        get: function (index) {
            return this.array[index];
        },

        move: function (oldIndex, newIndex) {
            var newArray = [];
            newArray[newIndex] = this.array[oldIndex];
            this.array[oldIndex] = undefined;

            this.rebuildArray(newArray);
        },

        rebuildArray: function (newArray) {
            "use strict";
            if (newArray === undefined) {
                newArray = [];
            }

            var newArrayIndex = 0;
            for (var i = 0; i < this.array.length; i++) {
                if (this.array[i] !== undefined) {
                    while (newArray[newArrayIndex] !== undefined) {
                        newArrayIndex++;
                    }

                    newArray[newArrayIndex] = this.array[i];

                    newArrayIndex++;
                }
            }

            this.array = newArray;
        }
    };

    return AudioPlaylist;
});