define([
    "js/audio/AudioPlaylist"
], function (AudioPlaylist) {
    var instance = null;

    function AudioPlayer() {
        if (instance !== null) {
            throw new Error("Cannot instantiate more than one AudioPlayer, use AudioPlayer.getInstance()");
        }

        this.currentPlaylist = new AudioPlaylist();
        this.volume = 1;
        this.currentTrackIndex = 0;
        this.currentTrack = undefined;
        this.progressInterval = undefined;
        this.controls = {};
    }

    AudioPlayer.getInstance = function () {
        if (instance === null) {
            instance = new AudioPlayer();
        }

        return instance;
    };

    AudioPlayer.prototype = {
        setControls: function (controlsContainer) {
            this.controls = {
                playPause: controlsContainer.querySelector(".playPause"),
                next: controlsContainer.querySelector(".next"),
                previous: controlsContainer.querySelector(".previous"),
                progressBar: controlsContainer.querySelector(".progressBar"),
                timeHandle: controlsContainer.querySelector(".timeHandle")
            };

            var that = this;
            this.controls.playPause.onclick = function () {
                that.togglePlayPause();
            };

            this.controls.next.onclick = function () {
                that.next();

            };
            this.controls.previous.onclick = function () {
                that.previous();
            };
        },

        setVolume: function (volume) {
            if (typeof volume !== "undefined") {
                this.volume = volume;
            }

            if (this.currentTrack !== undefined) {
                this.currentTrack.volume(this.volume);
            }

            return this;
        },

        play: function () {
            if (this.currentTrack === undefined) {
                this.currentTrack = this.currentPlaylist.get(this.currentTrackIndex);

                if (this.currentTrack === undefined) {
                    return;
                }
            }

            var that = this;

            this.currentTrack.onPlaying(function () {
                that.setVolume();
                that.updateControls();
                that.startInterval();
            });

            this.currentTrack.onEnded(function () {
                that.restartTrack();
                that.next(true);
            });

            this.currentTrack.play();

            return this;
        },

        next: function (playNow) {
            "use strict";

            if (!this.isPlayingPlaylist()) {
                return;
            }

            this.stop();
            if (this.repeat) {
                this.currentTrackIndex =
                    (this.currentTrackIndex + 1) % this.currentPlaylist.length();
            } else {
                this.currentTrackIndex++;
            }

            if (this.currentTrackIndex >= this.currentPlaylist.length()) {
                // When there is an overflow, replay last song
                this.currentTrackIndex--;
                this.currentTrack = this.currentPlaylist.get(this.currentTrackIndex);

                if (playNow === undefined) {
                    playNow = false;
                }

            } else if (this.currentPlaylist.get(this.currentTrackIndex) === undefined) {
                this.next();
            } else {
                this.currentTrack = this.currentPlaylist.get(this.currentTrackIndex);

                if (playNow !== false) {
                    this.play();
                }
            }
            return this;
        },

        previous: function (playNow) {
            "use strict";

            if (!this.isPlayingPlaylist()) {
                return;
            }

            this.stop();
            this.currentTrackIndex--;

            if (this.currentTrackIndex < 0) {
                if (this.repeat) {
                    this.currentTrackIndex = this.currentPlaylist.length() - 1;
                } else {
                    this.currentTrackIndex = 0;
                }
            }

            this.currentTrack = this.currentPlaylist.get(this.currentTrackIndex);

            if (playNow !== false) {
                this.play();
            }

            return this;
        },

        playTrack: function (audioTrack) {
            this.stop();
            this.clear();
            this.currentTrack = audioTrack;
            this.play();

            return this;
        },

        playTrackNumber: function (trackNumber) {
            "use strict";

            if (trackNumber < 0 || trackNumber >= this.currentPlaylist.length) {
                throw {name: "Invalid value", message: "The given track number " + trackNumber + "is not in range"};
            }

            if (this.currentTrack !== undefined) {
                this.currentTrack.stop();
            }

            this.currentTrackIndex = trackNumber;
            this.currentTrack = this.currentPlaylist.get(this.currentTrackIndex);

            this.play();

            return this;
        },

        pause: function () {
            if (this.currentTrack !== undefined) {
                this.currentTrack.pause();
                this.stopInterval();
                this.updateControls();
            }

            return this;
        },

        paused: function () {
            var paused = false;
            if (this.currentTrack === undefined) {
                paused = true;
            } else if (this.currentTrack.paused()) {
                paused = true;
            }

            return paused;
        },

        togglePlayPause: function () {
            if (this.paused()) {
                this.play();
            } else {
                this.pause();
            }

            return this;
        },

        stop: function () {
            this.pause();
            if (this.currentTrack !== undefined) {
                this.currentTrack.stop();
            }

            this.updateProgressBar();
            this.updateControls();

            return this;
        },

        clear: function () {
            this.currentPlaylist.empty();
            this.currentTrackIndex = 0;
            this.currentTrack = undefined;

            return this;
        },

        restartTrack: function () {
            this.currentTrack.currentTimePercent(0);
            this.updateProgressBar();
            this.updateControls();
            this.stopInterval();

            return this;
        },

        appendSong: function (audioTrack) {
            this.currentPlaylist.appendTrack(audioTrack);

            return this;
        },

        appendList: function (audioPlaylist) {
            this.currentPlaylist.appendItunesAlbum(audioPlaylist);

            return this;
        },

        replaceList: function (audioPlaylist) {
            if (this.paused() === false) {
                this.currentTrack.stop();
            }

            var newPlaylist = new AudioPlaylist();
            newPlaylist.appendItunesAlbum(audioPlaylist);
            this.currentTrackIndex = 0;
            this.currentTrack = newPlaylist.get(0);
            this.currentPlaylist = newPlaylist;
            return this;
        },

        updateControls: function () {
            this.controls.playPause.setAttribute("data-paused", this.paused());

            this.controls.timeHandle.setAttribute("data-beginning", this.currentTrack === undefined ||
            (this.currentTrack.atBeginning() && this.paused()));

            return this;
        },

        updateProgressBar: function () {
            "use strict";

            if (this.currentTrack !== undefined) {
                var ratio = this.currentTrack.currentTimePercent() * 100;
                this.controls.progressBar.style.width = ratio + "%";
            }

            return this;
        },

        startInterval: function () {
            "use strict";
            if (this.progressInterval !== undefined) {
                this.stopInterval();
            }

            var that = this;
            this.progressInterval = setInterval(function () {
                that.updateProgressBar();
            }, 40);

            return this;
        },

        stopInterval: function () {
            "use strict";
            clearInterval(this.progressInterval);
            this.progressInterval = undefined;

            return this;
        },

        currentTimePercent: function (percent) {
            "use strict";
            return this.currentTrack.currentTimePercent(percent);
        },

        isPlayingPlaylist: function () {
            return this.currentPlaylist.get(this.currentTrackIndex) === this.currentTrack;
        }
    };

    return AudioPlayer.getInstance();
});