define(function () {
    "use strict";

    var Utils = {
        "imgUrlTo400x400": function (url) {
            return url.replace("100x100", "400x400");
        },

        "imgUrlTo600x600": function (url) {
            return url.replace("100x100", "600x600");
        },

        "imgUrlTo1200x1200": function (url) {
            return url.replace("100x100", "1200x1200");
        },

        "transformMillisToMinutes": function (millis) {
            var date = new Date(millis);
            var seconds = (date.getSeconds() < 10) ? "0" + date.getSeconds() : date.getSeconds();

            return date.getMinutes() + ":" + seconds;
        },

        "keyCodes": {
            "ENTER": 13,
            "ARROW_UP": 38,
            "ARROW_DOWN": 40,
            "ESCAPE": 27
        },

        "TOKEN_COOKIE": "user-token",
        "USER_EMAIL": "user-email",
        "USER_ID": "user-id"
    };

    return Utils;
});
