define([
    "backbone",
    "js/views/homeView",
    "js/views/artist/artistView",
    "js/views/album/albumView",
    "js/views/playlist/allPlaylistView",
    "js/views/search/searchPageView",
    "js/views/user/userProfileView",
    "js/models/artistModel",
    "js/models/albumModel",
    "js/models/userModel",
    "js/collections/searchCollection",
    "js/collections/playlistCollection"
], function (Backbone, HomeView, ArtistView, AlbumView, AllPlaylistView, SearchPageView,
             UserProfileView, ArtistModel, AlbumModel, UserModel, SearchCollection, PlaylistCollection) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            "albums/:id": "getAlbum",
            "artists/:id": "getArtist",
            "playlists": "getPlaylists",
            "user/:id": "getUser",
            "login": "homeLogin",
            "research/:item": "getGlobalResearch",
            "research/:item/:filter": "getSpecificResearch",
            "*actions": "defaultRoot"
        },

        lastView: "",

        defaultRoot: function (actions) {
            this.cleanup();
            var homeView = new HomeView();
            homeView.render();
            this.setLastView(homeView);
        },

        homeLogin: function () {
            this.cleanup();
            var homeView = new HomeView();
            homeView.render();
            this.setLastView(homeView);
        },

        getAlbum: function (id) {
            this.cleanup();
            var albumModel = new AlbumModel({id: id});
            var albumView = new AlbumView({model: albumModel});
            albumModel.fetch();
            this.setLastView(albumView);
        },

        getArtist: function (id) {
            this.cleanup();
            var artistModel = new ArtistModel({id: id});
            var artistView = new ArtistView({model: artistModel});
            artistModel.fetch();
            this.setLastView(artistView);
        },

        getPlaylists: function () {
            this.cleanup();
            var playlistCollection = new PlaylistCollection();
            var allPlaylistView = new AllPlaylistView({collection: playlistCollection});
            this.setLastView(allPlaylistView);
        },

        getGlobalResearch: function (item) {
            this.getSpecificResearch(item);
        },

        getSpecificResearch: function (item, type) {
            this.cleanup();

            var formatedUrl = type ? "research/" + encodeURIComponent(item) + "/" + type : "research/" + encodeURIComponent(item);
            this.navigate(formatedUrl);

            var searchCollection = new SearchCollection({item: item, limit: 15, type: type});
            var searchPageView = new SearchPageView({collection: searchCollection});
            searchCollection.fetch();
            this.setLastView(searchPageView);
        },

        getUser: function (id) {
            this.cleanup();
            var userModel = new UserModel("/users/" + id);
            var playlistCollection = new PlaylistCollection();
            var userProfileView = new UserProfileView({
                model: userModel,
                collection: playlistCollection
            });
            userModel.fetch().done(function () {
                var email = userModel.get("email");
                playlistCollection.setUserEmail(email);
                playlistCollection.fetch();
            });

            this.setLastView(userProfileView);
        },

        cleanup: function () {
            if (this.lastView && this.lastView.cleanup) {
                this.lastView.cleanup();
            }
        },

        setLastView: function (view) {
            this.lastView = view;
        }
    });

    return AppRouter;
});