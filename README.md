Team 2
=====

## Membres de l'équipe

Nom                 |   IDUL        |   Matricule
------------------- | ------------- | -------------- 
Mathieu Carpentier  |   MACAR644    |   111 044 647
Jean-Samuel Bédard  |   JSBED5      |   111 043 631
Alexandre Gariépy   |   ALGAR73     |   111 046 788
Dario Martins Silva |   DAMAS54     |   111 050 189
Yohan Caron         |   YOCAR20     |   111 049 757
Jean-Gabriel Gill-Couture | JGGIC   |   111 045 527

## Fonctionnement

1. S'assurer que UBeat fonctionne sur https://ubeat.herokuapp.com ; Sinon vous pouvez l'héberger localement et modifier les urls
dans "resources/config.js" pour que l'application utilise l'API locale.

2. Utiliser WebStorm pour héberger les différents fichiers du dépôt sur un serveur HTTP local pour faire fonctionner le tout.

3. Ouvrir le fichier "resources/index.html" dans Chrome.


## Fonctionnalités avancées

1. Autocomplétion dans la barre de recherche.

2. Utilisation de [Gravatar](http://fr.gravatar.com/) pour le profile d'un utilisateur.


## Utilisation de l'url 'Unsecure'

Tout comme Youtube ou ITunes, UBeat offre la possibilité d'utiliser sa fonctionnalité de recherche
même si l'utilisateur n'est pas connecté. On peut donc 'browser' le contenu de Ubeat sans être connecté
(utilisation de l'url 'Unsecure').

Toutefois, pour toutes les autres fonctionnalités (utilisateurs, playlists, etc), Ubeat demande que l'utilisateur soit
connecté.