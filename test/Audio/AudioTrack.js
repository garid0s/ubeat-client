window.onload = function () {
    "use strict";

    var failedTests = 0, numberTests = 0, passedTests = 0;
    var track = new AudioTrack();

    var test = function (test, count) {

        if (count !== false) {
            numberTests++;
        }

        if (test) {
            passedTests++;
            console.log("Test #" + numberTests + " passed : OK");
        } else {
            var msg = "Test #" + numberTests + " failed";
            console.trace({name: 'TestFailedException', message: msg, track: track});
            failedTests++;
        }
    };

    try {

        test(track.audio !== undefined);

        test(typeof track.audio === "object");

        track.loadItunesTrack({
            "wrapperType": "track",
            "kind": "song",
            "artistId": 461932,
            "collectionId": 196480323,
            "trackId": 196480329,
            "artistName": "Europe",
            "collectionName": "The Final Countdown",
            "trackName": "The Final Countdown",
            "collectionCensoredName": "The Final Countdown",
            "trackCensoredName": "The Final Countdown",
            "artistViewUrl": "https://itunes.apple.com/us/artist/europe/id461932?uo=4",
            "collectionViewUrl": "https://itunes.apple.com/us/album/the-final-countdown/id196480323?i=196480329&uo=4",
            "trackViewUrl": "https://itunes.apple.com/us/album/the-final-countdown/id196480323?i=196480329&uo=4",
            "previewUrl": "http://a1815.phobos.apple.com/us/r1000/101/Music/70/f0/fd/mzm.hhpjhkpl.aac.p.m4a",
            "artworkUrl30": "http://a5.mzstatic.com/us/r30/Music/fc/4c/f5/mzi.jpmevzoi.30x30-50.jpg",
            "artworkUrl60": "http://a4.mzstatic.com/us/r30/Music/fc/4c/f5/mzi.jpmevzoi.60x60-50.jpg",
            "artworkUrl100": "http://a3.mzstatic.com/us/r30/Music/fc/4c/f5/mzi.jpmevzoi.100x100-75.jpg",
            "collectionPrice": 9.99,
            "trackPrice": 1.29,
            "releaseDate": "1988-09-16T07:00:00Z",
            "collectionExplicitness": "notExplicit",
            "trackExplicitness": "notExplicit",
            "discCount": 1,
            "discNumber": 1,
            "trackCount": 13,
            "trackNumber": 1,
            "trackTimeMillis": 310333,
            "country": "USA",
            "currency": "USD",
            "primaryGenreName": "Rock",
            "radioStationUrl": "https://itunes.apple.com/station/idra.196480329"
        });

        test(track.getItunesParameter('country') === 'USA');
        test(track.getItunesParameter('currency') === 'USD');
        test(track.getItunesParameter('trackPrice') === 1.29);

        test(track.audio.src === "http://a1815.phobos.apple.com/us/r1000/101/Music/70/f0/fd/mzm.hhpjhkpl.aac.p.m4a");

        track.play();
        test(track.paused() === false);
        track.pause();
        test(track.paused() === true);


        var failed = false;
        try {
            track.currentTime(3);
        } catch (e) {
            failed = true;
        }
        test(failed); // Have to wait for the audio to be ready before messing with the currentTime

        test(track.currentTime() === 0);
        track.stop();
        test(track.currentTime() === 0);

        var canplayCallBack = function () {
            test(track.readyState() > 0);
            test(track.currentTime(3) === 3);

            track.play();
            test(track.paused() === false);

            setTimeout(function () {
                test(track.currentTime() > 3);
                track.pause();
                test(track.paused() === true);
            }, 1000);

        };

        track.canPlayThrough(canplayCallBack);
        test(typeof track.audio.oncanplaythrough === "function");

        track.canPlay(canplayCallBack);
        test(typeof track.audio.oncanplay === "function");
    } catch (e) {
        console.trace(e);
        console.log("UNABLE TO COMPLETE TEST SUITE SUCCESSFULLY");
    } finally {
        console.log("LINEAR TEST SUITE COMPLETED. Callback tests following. " + passedTests + "/" + numberTests + " passed.");
    }
};

